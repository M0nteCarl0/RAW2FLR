#pragma once
#include <fstream>
#include <stdint.h>
using namespace std;
#ifndef _FLR_POSIX_
#define _FLR_POSIX_
/*************************************************************************************************
*                 ������ ��� ������ � FLR ��������(POSIX)
*                 ���������� �.�(molotatliev@xprom.ru)
*                 Initial version:13.10.15
*
*
*
*
***************************************************************************************************/

typedef struct FLE_HEADER
{
uint16_t	_fType;
uint16_t	_Width;
uint16_t	_fHeight;
uint16_t	_fBpp;
uint16_t	_BitsOffset;
}FLE_HEADER,&FLE_HEADER_R,*FLE_HEADER_P;

class FLR_P
{
public:
	FLR_P(void);
	~FLR_P(void);
    bool Make(const char* Filename,const char* RawSource);
	void SetResolution(uint16_t Resolution);
    void Make2(const char* Filename,const char* RawSource,const char* Filename1,const char* RawSource1);
    void Make(const char* Filename,uint16_t* Data,size_t Datasize);
    void Make2(const char* Filename,uint16_t* Data,size_t Datasize,const char* Filename1,uint16_t* Data1,size_t Datasize1);
	void Open(const char* Filename);
	void ReadImage(uint16_t* Data,size_t& Datasize);
	void WriteData(uint16_t* Data,size_t Datasize);
	void ReadData(uint16_t* Data,size_t Datasize);
	void WriteHeader(FLE_HEADER_R Head);
	void ReadHeader(FLE_HEADER_R Head);
	size_t GetFileSize(void);
private:
	uint16_t _Resolution;
	uint16_t _SizeImage;
	uint16_t* _ImageBuff;
	fstream  _FIO;
	string	 _FileName;
	template<typename T>  bool  Check(T* In,T* Out,size_t Count);
};
#endif
