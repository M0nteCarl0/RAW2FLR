// Raw2lFLR.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "Raw2lFLR.h"
#include "FLR_P.h"
#include "RAW_P.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ������������ ������ ����������

CWinApp theApp;

using namespace std;
FLR_P Maker;
RAW_P RAW_mage;
void Process(int argc, TCHAR* argv[]);
void Make(void);
int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// ���������������� MFC, � ����� ������ � ��������� �� ������� ��� ����
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: �������� ��� ������ �������������� ����� ������������
			_tprintf(_T("����������� ������: ���� ��� ������������� MFC\n"));
			nRetCode = 1;
		}
		else
		{
		 Process(argc, argv);
		 system("pause");
			// TODO: �������� ���� ��� ��� ����������.
		}
	}
	else
	{
		// TODO: �������� ��� ������ �������������� ����� ������������
		_tprintf(_T("����������� ������: ��������� ���������� GetModuleHandle\n"));
		nRetCode = 1;
	}

	return nRetCode;
}


void RemExt(string &In)
{
	string TMP = In;
	size_t PositionBegin = In.length() - 4;
	size_t RT =  TMP.find(".",PositionBegin);

	if(RT!=string::npos )
	{
		
		TMP.erase(RT,TMP.length());
		In = TMP;
		
	}

}


bool InputIsFLR(string &In)
{

	bool flag = false;

	string TMP;


	size_t PositionBegin = In.length() - 4;
	TMP =  string(In,PositionBegin,In.length());
	string Extension;
	size_t LenghExt;
	size_t RT =  TMP.find(".");
	if(RT!=string::npos )
	{
	//	std::cout << "DOT POS" << RT <<"\n";
		LenghExt = In.length() - RT;
		Extension = string(TMP,RT,LenghExt);
		std::cout << "Extensiion Input file " << TMP.c_str()  <<"\n";
		if(Extension .compare(".flr") == 0 || (Extension .compare(".FLR") == 0))
		{

			flag = true;

		}


	}
	

	return flag;
}


void Process(int argc, TCHAR* argv[])
{
	
	//if(argc < 3)
//	{

	//	std::cout << "Wrong in Param!";
		//system("pause");
	//	//exit(128);
	//} 



		if(argc == 2)
		{
			string OutNameIn1 = argv[1];
			
		
			string OutNameIn2 = OutNameIn1;

			if(!InputIsFLR(OutNameIn1))
			{
				cout << "Convert RAW2FLR mode\n";
				if(!RAW_mage.Open(OutNameIn1.c_str()))
				{

					std::cout << "Can not open file \n";
					RAW_mage.Close();

				}
				else
				{
					RemExt( OutNameIn1);
					if(Maker.Make(OutNameIn1.c_str(),RAW_mage.GetDataBuffer(),RAW_mage.GetFileSize()))
					{
							std::cout << "Convrt OK \n";

					}
					else
					{
							std::cout << "Convrt fail \n";

					};

                    RAW_mage.Close();

				}




			}
			else
			{
				
				cout << "Convert FLR2RAW mode\n";
				if(Maker.Open(OutNameIn1.c_str()))
				{
					RemExt( OutNameIn1);
					if(RAW_mage.Make(OutNameIn1.c_str(),Maker.GetDataBuffer(),Maker.GetImageSize()))
					{
							std::cout << "Convrt OK \n";
							Maker.Close();
					}
					else
					{
						std::cout << "Convrt fail \n";

					}

				}
				else
				{
					std::cout << "Can not open file \n";
					Maker.Close();

				}



			}

#if 0
			if(Maker.Make(OutNameIn1.c_str(),OutNameIn2.c_str()))
			{
				std::cout << "Convert OK!\n";
				std::cout << "Out File: " << OutNameIn1.c_str() << " .flr\n";
			}
			else
			{
				std::cout << "Convert Not Completed !\n";

			}
#endif
		}



		if(argc  == 3)
		{
			std::cout << "Input File: " << argv[1]<< "\n";
			string OutNameIn1 = argv[1];
			string OutNameIn2 = argv[2];
			RemExt(OutNameIn1);
			RemExt(OutNameIn2);
			if(Maker.Make(argv[2],argv[1]))
			{
				std::cout << "Convert OK!\n";
				std::cout << "Out File: " << argv[2] << " .flr\n";
			}
			else
			{
				std::cout << "Convert Not Completed !\n";

			}
		
		}


	}

