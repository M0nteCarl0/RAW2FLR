#include "FLR_P.h"
#include <Windows.h>
#define FLR_HEADER_MARKER      ((uint16_t) ('R' << 8) | 'F')
static uint64_t  _ObjectCounter = 0;;
CRITICAL_SECTION _SE;
	/************************************************/
	FLR_P::FLR_P(void):_Resolution(0)
	{
		InitializeCriticalSection(&_SE);
		_ObjectCounter++;
	}
	/************************************************/
	FLR_P::~FLR_P(void)
	{
		//DebugBreak();

	//	_ObjectCounter--;
		//CloseHandle(&_SE);
		if(_ImageBuff!=NULL)
		{
			delete[] _ImageBuff;
		}
	}
	/************************************************/
	bool FLR_P:: Make(const char* Filename,const char* RawSource)
	{
		uint16_t* ImageBuff;
		bool FlagOk = true;
		string Finput = RawSource;
		string Fout = Filename;
		Finput+=".raw";
		Fout  +=".flr";
		FLE_HEADER FH;
		FH._fBpp = 16;
		FH._BitsOffset = 0;
		FH._fType  = FLR_HEADER_MARKER;
		_FIO = fstream(Finput,std::ios::in|std::ios::binary|std::ios::ate);
		if(!_FIO.fail())
		{
	     size_t _FSS =(size_t) _FIO.tellg();
		 ImageBuff = new uint16_t[_FSS/2]; 
		 _FIO.close();
		 _FIO = fstream(Finput,std::ios::in|std::ios::binary);
		 size_t ImageSize = (size_t)sqrt((double)_FSS /sizeof(uint16_t));
		FH._fHeight = FH._Width = ImageSize;
		_FIO.read((char*)ImageBuff,_FSS);
		_FIO.close();
		_FIO.open(Fout,std::ios::out|std::ios::binary);
		WriteHeader(FH);
		WriteData(ImageBuff,_FSS);
		_FIO.close();
		delete[] ImageBuff;
		}
		else
		{
		FlagOk = false;
		}

		return FlagOk;

	}
	/************************************************/
	void FLR_P::Make(const char* Filename,uint16_t* Data,size_t Datasize)
	{
		EnterCriticalSection(&_SE);
		string Fout = Filename;
		uint16_t* ChData = new uint16_t[Datasize/2];
		Fout  +=".flr";
		FLE_HEADER FH;
		FLE_HEADER FC;
		FH._fBpp = 16;
		FH._BitsOffset = 0;
		FH._fType  = FLR_HEADER_MARKER;
		size_t ImageSize =(size_t)  sqrt((double)Datasize /sizeof(uint16_t));
		FH._fHeight = FH._Width = ImageSize;
		_FIO.open(Fout,std::ios::out|std::ios::binary);
		WriteHeader(FH);
		WriteData(Data,Datasize);
		_FIO.close();
		_FIO.open(Fout,std::ios::in|std::ios::binary);
		ReadHeader(FC);
		ReadData(ChData,Datasize);
		_FIO.close();
		if(!Check(ChData,Data,Datasize/2))
		{

			printf("File  %s -> Data corupted!\n",Filename);
		}
		delete [] ChData;
		LeaveCriticalSection(&_SE);

	}
	/************************************************/
	void  FLR_P::Make2(const char* Filename,uint16_t* Data,size_t Datasize,const char* Filename1,uint16_t* Data1,size_t Datasize1)
	{
		Make(Filename,Data,Datasize);
	    Make(Filename1,Data1,Datasize1);
	}
	/************************************************/
	void FLR_P::Make2(const char* Filename,const char* RawSource,const char* Filename1,const char* RawSource1)
	{
		Make(Filename,RawSource);
	    Make(Filename1,RawSource1);

	}
	/************************************************/
	void FLR_P::Open(const char* Filename)
	    {
	     EnterCriticalSection(&_SE);
		 uint16_t FileSize = 0;
		 FLE_HEADER _HeadF;
		 memset(&_HeadF,0,sizeof(FLE_HEADER));
		_FileName = Filename;
		_FileName+=".flr";
	
		_FIO = fstream(_FileName.c_str(),std::ios::in|std::ios::binary);
		ReadHeader(_HeadF);
		//_SizeImage = _HeadF._fHeight * _HeadF._Width;
	   _ImageBuff = new uint16_t[_HeadF._fHeight * _HeadF._Width];
		ReadData(_ImageBuff,(_HeadF._fHeight * _HeadF._Width)*2);
		LeaveCriticalSection(&_SE);
	
	}

	/************************************************/
	void FLR_P::ReadHeader(FLE_HEADER_R Head)
	{
		_FIO.read((char*)&Head,sizeof(Head));
		uint16_t W;
		uint16_t H;
		///memcpy(&W,&Head._Width,sizeof(uint16_t));
		//memcpy(&H,&Head._fHeight,sizeof(uint16_t));
		//_SizeImage = W*H;
	}

	/************************************************/
	void FLR_P::ReadData(uint16_t* Data,size_t Datasize)
	{
		_FIO.read((char*)Data,Datasize);
	}
	/************************************************/
	size_t FLR_P::GetFileSize(void)
	{
		return(size_t)_FIO.tellg();
	}
	/************************************************/
	template<typename T>  bool FLR_P:: Check(T* In,T* Out,size_t Count)
	{
		bool Res = true;
		size_t ERC = 0;
		for(size_t i = 0;i<Count;i++)
		{
			if(In[i]!=Out[i])
			{

				ERC++;
			}
		
       }
		if(ERC!=0)
		{
			Res = false;

		}
		return Res;
	}
	/************************************************/
	void FLR_P:: WriteHeader(FLE_HEADER_R Head)
	{
		_FIO.write((char*)&Head,sizeof(Head));
	}
	/************************************************/
	void FLR_P:: WriteData(uint16_t* Data,size_t Datasize)
	{
		_FIO.write((char*)Data,Datasize);
	}
	/************************************************/
	void FLR_P::ReadImage(uint16_t* Data,size_t& Datasize)
	{
		memcpy(Data,_ImageBuff,3328*3328*2);
		Datasize = _SizeImage; 
	}
	/************************************************/